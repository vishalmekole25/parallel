##################################################################################
# Function to send a slack notification
# 1st param - Slack Message e.g "This is my slack message!"
# 2nd param - Slack channel webhook e.g. "https://hooks.slack.com/services/..."
# Usage: slackNotify "Deployment Successful!" "<slack-channel-webhook-url>"
##################################################################################

#
# ->PRIVATE<- funtions:  Support functions for slackNotify
#

wget_encode () {
    # wget_encode <string>

    string="${1}"
    encoded=""

    encoded=$(echo "$string" | sed -E 's/["]+/\\\"/g')
    echo "${encoded}"
}

#
# END ->PRIVATE<- support functions for slackNotify
#

slackNotify () {
    # Check for mandatory arguments and fail if not found.
    [ -z "$1" ] && echo "[ERROR] SLACK MESSAGE as 1st argument to slackNotify fn. is needed along with SLACK CHANNEL WEBHOOK as 2nd argument." && exit 1;
    [ -z "$2" ] && echo "[ERROR] SLACK CHANNEL WEBHOOK as 2nd argument to slackNotify fn. is needed." && exit 1;
    echo "[INFO] ===> [TMOGF]: Slack Notification Requested!!! "
    # Check Dependency
    if ! exists "wget" ; then installPackage "wget"; fi;

    # Encode message so wget remains happy if characters like double-quotes are otherwise encountered
    ENCODED_MESSAGE=$(wget_encode "$1")

    # Post SLACK MESSAGE ($1) via WEBHOOK ($2) using wget
    {
      wget -O- -q --header='Content-Type:application/json' --post-data='{"text":"'"$ENCODED_MESSAGE"'"}' "$2"
    } || {
        exit_code=$?
        echo "Error occurred while triggering the requested slack notification (wget exit code: $exit_code)!!!" "warn"
    }
}

